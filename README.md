# Self Driving Cars - 2024



## Name

This repo is maintained for different project I have worked or will be working for self driving car's space.

## Description

Learn by doing -

Project 1 - Car Detection, tracking and Counting using YOLO library

## Badges

On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals

Will add output images of all the projects here -

Project 1 - Car detection and counting using YOLO


![](C:\Users\rkapse\AppData\Roaming\marktext\images\2024-04-21-11-35-54-image.png)

## 

## Roadmap

If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing

Open to contribute and use.

## Authors and acknowledgment

Ritesh Kapse

Shubham

## License

For open source projects, say how it is licensed.

## Project status

Ongoing learnings and Development using Jetson Nano + OKD-R lite kit
