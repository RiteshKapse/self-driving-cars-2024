from yolo import YoloDetection
import cv2
import time
import argparse
from centroid_tracker import Tracker
# import imageio

CONFIG_FILE = None
model = None

def load_config(config_path):
    global CONFIG_FILE
    CONFIG_FILE = eval(open(config_path).read())


def load_model():
    global model
    model = YoloDetection(CONFIG_FILE["model-parameters"]["model-weights"],
                    CONFIG_FILE["model-parameters"]["model-config"],
                    CONFIG_FILE["model-parameters"]["model-names"],
                    CONFIG_FILE["shape"][0],
                    CONFIG_FILE["shape"][1])



def start_detection(media_path):

    tracker = Tracker()

    cv2.namedWindow("Video",cv2.WINDOW_NORMAL)
    cap = cv2.VideoCapture(media_path)
    # writer = imageio.get_writer("demo1.mp4")
    ret = True
    while ret:
        start_time=time.time()
        ret , frame = cap.read()

        detections = model.process_frame(frame)
        tracker_res = tracker.update_object([ x[1:5] for x in detections ])

        
        for id,boxes in tracker_res.items():
            x,y = (int(boxes[0]), int(boxes[1]))
            w,h = (int(boxes[2]), int(boxes[3]))
            cv2.rectangle(frame,(x,y),(x+w,y+h),thickness=3,color=(254, 118, 136))
            cv2.rectangle(frame,(x,y-5),(x+30,y+10),thickness=-1,color=(254, 118, 136))
            cv2.putText(frame,"ID: "+str(id),(x,y+5),cv2.FONT_HERSHEY_SIMPLEX, 0.3, (255,255,255), 1)

        
        cv2.rectangle(frame, (0, 10), (110, 30), (195,68,60), -1)
        cv2.putText(frame, "FPS : {}".format(int(1/(time.time()-start_time))), (13, 23), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255,255,255), 2)

        cv2.rectangle(frame, (350, 10), (210, 30), (84,61,246), -1)
        cv2.putText(frame, "Cars in : {}".format(len(tracker_res)), (213, 23), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255,255,255), 2)

        cv2.rectangle(frame, (500, 10), (700, 30), (255,127,2), -1)
        cv2.putText(frame, "Cars out : {}".format(len(tracker_res)-4), (503, 23), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255,255,255), 2)
        
        cv2.imshow("Video",frame)
        
        key = cv2.waitKey(30)
        if(key==27):
            break
    # writer.close()
    cv2.destroyAllWindows()

if __name__=="__main__":
    out = cv2.VideoWriter('output.mp4', -1, 20.0, (640,480))
    parser = argparse.ArgumentParser(description="Provide arguements")
    parser.add_argument("--config","-c")
    parser.add_argument("--debug","-d")
    parser.add_argument("--video","-v")
    args = parser.parse_args()
    config_path = args.config
    load_config(config_path)
    load_model()
    start_detection(args.video)